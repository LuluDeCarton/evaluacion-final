package cl.loverandom.evaluados;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.loverandom.evaluados.R;

public class PestaniaTres extends Fragment {

    EditText etUsername, etPassword;
    Button btFinal;

    public PestaniaTres() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_pestania_tres,container,false);

        etUsername = (EditText)view.findViewById(R.id.etUsername);
        etPassword = (EditText)view.findViewById(R.id.etPassword);
        btFinal = (Button)view.findViewById(R.id.btFinal);

        btFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String user = etUsername.getText().toString();
                String pass = etPassword.getText().toString();

                if(!user.isEmpty() && !pass.isEmpty()){

                    Toast.makeText(getActivity(), "Su Contraseña es: " +pass, Toast.LENGTH_SHORT).show();


                }else{

                    Toast.makeText(getActivity(), "Los campos son obligatorios.", Toast.LENGTH_SHORT).show();

                }

            }
        });


        return view;
    }

}
