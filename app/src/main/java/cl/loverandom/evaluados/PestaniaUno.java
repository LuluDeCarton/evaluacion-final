package cl.loverandom.evaluados;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.loverandom.evaluados.R;

public class PestaniaUno extends Fragment {

    Button btNextOne;
    EditText etNombre, etEdad;

    public PestaniaUno() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_pestania_uno,container,false);

        etNombre = (EditText)view.findViewById(R.id.etNombre);
        etEdad = (EditText)view.findViewById(R.id.etEdad);
        btNextOne = (Button) view.findViewById(R.id.btNextOne);

        btNextOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = etNombre.getText().toString();
                String old =  etEdad.getText().toString();

                if(!name.isEmpty() ){

                    FragmentTransaction fr = getFragmentManager().beginTransaction();
                    fr.replace(R.id.fragment_container, new PestaniaDos ());
                    fr.commit();

                }else{
                    Toast.makeText(getActivity(), "El nombre es obligatorio", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

}
